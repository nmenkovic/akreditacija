/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package konverter;

import db.Kontroler;
import domen.Katedra;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Komp
 */
@FacesConverter(forClass = Katedra.class, value = "katedraKonverter")
public class KatedraKonventer implements Converter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
       if (value != null) {
            try {
                int katedraId = Integer.parseInt(value);
                List<Katedra> katedre = Kontroler.vratiObjekat().vratiSveKatedre();
                for (Katedra k : katedre ) {
                    if (k.getId()== katedraId) {
                        return k;
                    }
                }
                return null;
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else {
            return new Katedra();
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
          return String.valueOf(((Katedra) value).getId());
    }
    
}
