/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package konverter;

import db.Kontroler;
import domen.Titula;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Komp
 */
@FacesConverter(forClass = Titula.class, value = "titulaKonverter")
public class TitulaKonverter implements Converter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null) {
            try {
                int titulaId = Integer.parseInt(value);
                List<Titula> titule = Kontroler.vratiObjekat().vratiSveTitule();
                for (Titula t : titule) {
                    if (t.getId()== titulaId) {
                        return t;
                    }
                }
                return null;
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        } else {
            return new Titula();
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
       return String.valueOf(((Titula) value).getId());
    }
    
}
