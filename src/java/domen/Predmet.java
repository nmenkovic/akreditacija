/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NMenkovic
 */
@Entity
@Table(name = "predmet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Predmet.findAll", query = "SELECT p FROM Predmet p"),
    @NamedQuery(name = "Predmet.findById", query = "SELECT p FROM Predmet p WHERE p.id = :id"),
    @NamedQuery(name = "Predmet.findByOznaka", query = "SELECT p FROM Predmet p WHERE p.oznaka = :oznaka"),
    @NamedQuery(name = "Predmet.findByBrojEspb", query = "SELECT p FROM Predmet p WHERE p.brojEspb = :brojEspb")})
public class Predmet implements Serializable, OpstiDomenskiObjekat {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "naziv")
    private String naziv;
    @Size(max = 50)
    @Column(name = "oznaka")
    private String oznaka;
    @Column(name = "broj_espb")
    private Integer brojEspb;
    @ManyToMany(mappedBy = "predmetList")
    private List<Nastavnik> nastavnikList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "predmetId")
    private List<Oblast> oblastList;

    public Predmet() {
    }

    public Predmet(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOznaka() {
        return oznaka;
    }

    public void setOznaka(String oznaka) {
        this.oznaka = oznaka;
    }

    public Integer getBrojEspb() {
        return brojEspb;
    }

    public void setBrojEspb(Integer brojEspb) {
        this.brojEspb = brojEspb;
    }

    @XmlTransient
    public List<Nastavnik> getNastavnikList() {
        return nastavnikList;
    }

    public void setNastavnikList(List<Nastavnik> nastavnikList) {
        this.nastavnikList = nastavnikList;
    }

    @XmlTransient
    public List<Oblast> getOblastList() {
        return oblastList;
    }

    public void setOblastList(List<Oblast> oblastList) {
        this.oblastList = oblastList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Predmet)) {
            return false;
        }
        Predmet other = (Predmet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Predmet[ id=" + id + " ]";
    }

    @Override
    public String vratiNazivKljuca() {
        return "id";
    }

    @Override
    public String vratiNazivTabele() {
        return "predmet";
    }

    @Override
    public Object vratiVrednostKljuca() {
        return id;
    }
    
}
