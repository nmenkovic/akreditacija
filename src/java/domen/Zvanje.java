/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NMenkovic
 */
@Entity
@Table(name = "zvanje")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Zvanje.findAll", query = "SELECT z FROM Zvanje z"),
    @NamedQuery(name = "Zvanje.findById", query = "SELECT z FROM Zvanje z WHERE z.id = :id")})
public class Zvanje implements Serializable, OpstiDomenskiObjekat {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Lob
    @Column(name = "naziv")
    private byte[] naziv;
    @Lob
    @Column(name = "skraceni_naziv")
    private byte[] skraceniNaziv;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "zvanjeId")
    private List<Nastavnikzvanje> nastavnikzvanjeList;
    @OneToMany(mappedBy = "zvanjeId")
    private List<Nastavnik> nastavnikList;

    public Zvanje() {
    }

    public Zvanje(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getNaziv() {
        return naziv;
    }

    public void setNaziv(byte[] naziv) {
        this.naziv = naziv;
    }

    public byte[] getSkraceniNaziv() {
        return skraceniNaziv;
    }

    public void setSkraceniNaziv(byte[] skraceniNaziv) {
        this.skraceniNaziv = skraceniNaziv;
    }

    @XmlTransient
    public List<Nastavnikzvanje> getNastavnikzvanjeList() {
        return nastavnikzvanjeList;
    }

    public void setNastavnikzvanjeList(List<Nastavnikzvanje> nastavnikzvanjeList) {
        this.nastavnikzvanjeList = nastavnikzvanjeList;
    }

    @XmlTransient
    public List<Nastavnik> getNastavnikList() {
        return nastavnikList;
    }

    public void setNastavnikList(List<Nastavnik> nastavnikList) {
        this.nastavnikList = nastavnikList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zvanje)) {
            return false;
        }
        Zvanje other = (Zvanje) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Zvanje[ id=" + id + " ]";
    }

    @Override
    public String vratiNazivKljuca() {
        return "id";
    }

    @Override
    public String vratiNazivTabele() {
     return "zvanje";    
    }

    @Override
    public Object vratiVrednostKljuca() {
        return id;
    }
    
}
