/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NMenkovic
 */
@Entity
@Table(name = "katedra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Katedra.findAll", query = "SELECT k FROM Katedra k"),
    @NamedQuery(name = "Katedra.findById", query = "SELECT k FROM Katedra k WHERE k.id = :id"),
    @NamedQuery(name = "Katedra.findByNaziv", query = "SELECT k FROM Katedra k WHERE k.naziv = :naziv")})
public class Katedra implements Serializable, OpstiDomenskiObjekat {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "naziv")
    private String naziv;
    @OneToMany(mappedBy = "katedraId")
    private List<Nastavnik> nastavnikList;

    public Katedra() {
    }

    public Katedra(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @XmlTransient
    public List<Nastavnik> getNastavnikList() {
        return nastavnikList;
    }

    public void setNastavnikList(List<Nastavnik> nastavnikList) {
        this.nastavnikList = nastavnikList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Katedra)) {
            return false;
        }
        Katedra other = (Katedra) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return naziv;
    }

    @Override
    public String vratiNazivKljuca() {
        return "id";
    }

    @Override
    public String vratiNazivTabele() {
        return "katedra";
    }

    @Override
    public Object vratiVrednostKljuca() {
        return id;
    }
    
}
