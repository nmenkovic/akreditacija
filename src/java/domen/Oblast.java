/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NMenkovic
 */
@Entity
@Table(name = "oblast")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Oblast.findAll", query = "SELECT o FROM Oblast o"),
    @NamedQuery(name = "Oblast.findById", query = "SELECT o FROM Oblast o WHERE o.id = :id")})
public class Oblast implements Serializable, OpstiDomenskiObjekat {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "naziv")
    private String naziv;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "opis")
    private String opis;
    @OneToMany(mappedBy = "nadoblastId")
    private List<Oblast> oblastList;
    @JoinColumn(name = "nadoblast_id", referencedColumnName = "id")
    @ManyToOne
    private Oblast nadoblastId;
    @JoinColumn(name = "predmet_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Predmet predmetId;

    public Oblast() {
    }

    public Oblast(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @XmlTransient
    public List<Oblast> getOblastList() {
        return oblastList;
    }

    public void setOblastList(List<Oblast> oblastList) {
        this.oblastList = oblastList;
    }

    public Oblast getNadoblastId() {
        return nadoblastId;
    }

    public void setNadoblastId(Oblast nadoblastId) {
        this.nadoblastId = nadoblastId;
    }

    public Predmet getPredmetId() {
        return predmetId;
    }

    public void setPredmetId(Predmet predmetId) {
        this.predmetId = predmetId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Oblast)) {
            return false;
        }
        Oblast other = (Oblast) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Oblast[ id=" + id + " ]";
    }

    @Override
    public String vratiNazivKljuca() {
        return "id";
    }

    @Override
    public String vratiNazivTabele() {
        return "oblast";
    }

    @Override
    public Object vratiVrednostKljuca() {
        return id;
    }
    
}
