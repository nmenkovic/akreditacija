/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domen;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NMenkovic
 */
@Entity
@Table(name = "titula")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Titula.findAll", query = "SELECT t FROM Titula t"),
    @NamedQuery(name = "Titula.findById", query = "SELECT t FROM Titula t WHERE t.id = :id"),
    @NamedQuery(name = "Titula.findByNaziv", query = "SELECT t FROM Titula t WHERE t.naziv = :naziv"),
    @NamedQuery(name = "Titula.findBySkraceniNaziv", query = "SELECT t FROM Titula t WHERE t.skraceniNaziv = :skraceniNaziv")})
public class Titula implements Serializable, OpstiDomenskiObjekat {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "naziv")
    private String naziv;
    @Size(max = 50)
    @Column(name = "skraceni_naziv")
    private String skraceniNaziv;
    @OneToMany(mappedBy = "titulaId")
    private List<Nastavnik> nastavnikList;

    public Titula() {
    }

    public Titula(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getSkraceniNaziv() {
        return skraceniNaziv;
    }

    public void setSkraceniNaziv(String skraceniNaziv) {
        this.skraceniNaziv = skraceniNaziv;
    }

    @XmlTransient
    public List<Nastavnik> getNastavnikList() {
        return nastavnikList;
    }

    public void setNastavnikList(List<Nastavnik> nastavnikList) {
        this.nastavnikList = nastavnikList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Titula)) {
            return false;
        }
        Titula other = (Titula) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return naziv;
    }

    @Override
    public String vratiNazivKljuca() {
        return "id";
    }

    @Override
    public String vratiNazivTabele() {
        return "titula";
    }

    @Override
    public Object vratiVrednostKljuca() {
        return id;
    }
}
