/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NMenkovic
 */
@Entity
@Table(name = "nastavnikzvanje")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nastavnikzvanje.findAll", query = "SELECT n FROM Nastavnikzvanje n"),
    @NamedQuery(name = "Nastavnikzvanje.findById", query = "SELECT n FROM Nastavnikzvanje n WHERE n.id = :id"),
    @NamedQuery(name = "Nastavnikzvanje.findByDatumIzbora", query = "SELECT n FROM Nastavnikzvanje n WHERE n.datumIzbora = :datumIzbora"),
    @NamedQuery(name = "Nastavnikzvanje.findByInstitucija", query = "SELECT n FROM Nastavnikzvanje n WHERE n.institucija = :institucija")})
public class Nastavnikzvanje implements Serializable, OpstiDomenskiObjekat {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Column(name = "datum_izbora")
    private BigInteger datumIzbora;
    @Size(max = 50)
    @Column(name = "institucija")
    private String institucija;
    @JoinColumn(name = "zvanje_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Zvanje zvanjeId;
    @JoinColumn(name = "nastavnik_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Nastavnik nastavnikId;

    public Nastavnikzvanje() {
    }

    public Nastavnikzvanje(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigInteger getDatumIzbora() {
        return datumIzbora;
    }

    public void setDatumIzbora(BigInteger datumIzbora) {
        this.datumIzbora = datumIzbora;
    }

    public String getInstitucija() {
        return institucija;
    }

    public void setInstitucija(String institucija) {
        this.institucija = institucija;
    }

    public Zvanje getZvanjeId() {
        return zvanjeId;
    }

    public void setZvanjeId(Zvanje zvanjeId) {
        this.zvanjeId = zvanjeId;
    }

    public Nastavnik getNastavnikId() {
        return nastavnikId;
    }

    public void setNastavnikId(Nastavnik nastavnikId) {
        this.nastavnikId = nastavnikId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nastavnikzvanje)) {
            return false;
        }
        Nastavnikzvanje other = (Nastavnikzvanje) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Nastavnikzvanje[ id=" + id + " ]";
    }

    @Override
    public String vratiNazivKljuca() {
        return "id";
    }

    @Override
    public String vratiNazivTabele() {
        return "nastavnikzvanje";
    }

    @Override
    public Object vratiVrednostKljuca() {
        return id;
    }
    
}
