/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domen;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NMenkovic
 */
@Entity
@Table(name = "nastavnik")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nastavnik.findAll", query = "SELECT n FROM Nastavnik n"),
    @NamedQuery(name = "Nastavnik.findById", query = "SELECT n FROM Nastavnik n WHERE n.id = :id"),
    @NamedQuery(name = "Nastavnik.findByDatumRodjenja", query = "SELECT n FROM Nastavnik n WHERE n.datumRodjenja = :datumRodjenja"),
    @NamedQuery(name = "Nastavnik.findByBrojTelefona", query = "SELECT n FROM Nastavnik n WHERE n.brojTelefona = :brojTelefona"),
    @NamedQuery(name = "Nastavnik.findByEmailAdresa", query = "SELECT n FROM Nastavnik n WHERE n.emailAdresa = :emailAdresa"),
    @NamedQuery(name = "Nastavnik.findByRedniBroj", query = "SELECT n FROM Nastavnik n WHERE n.redniBroj = :redniBroj")})
public class Nastavnik implements Serializable, OpstiDomenskiObjekat {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ime_prezime")
    private String imePrezime;
    @Column(name = "datum_rodjenja")
    private BigInteger datumRodjenja;
    @Size(max = 50)
    @Column(name = "broj_telefona")
    private String brojTelefona;
    @Size(max = 50)
    @Column(name = "email_adresa")
    private String emailAdresa;
    @Column(name = "redni_broj")
    private Integer redniBroj;
    @JoinTable(name = "nastavnikpredmet", joinColumns = {
        @JoinColumn(name = "nastavnik_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "predmet_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Predmet> predmetList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nastavnikId")
    private List<Nastavnikzvanje> nastavnikzvanjeList;
    @JoinColumn(name = "katedra_id", referencedColumnName = "id")
    @ManyToOne
    private Katedra katedraId;
    @JoinColumn(name = "zvanje_id", referencedColumnName = "id")
    @ManyToOne
    private Zvanje zvanjeId;
    @JoinColumn(name = "titula_id", referencedColumnName = "id")
    @ManyToOne
    private Titula titulaId;

    public Nastavnik() {
    }

    public Nastavnik(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImePrezime() {
        return imePrezime;
    }

    public void setImePrezime(String imePrezime) {
        this.imePrezime = imePrezime;
    }

    public BigInteger getDatumRodjenja() {
        return datumRodjenja;
    }

    public void setDatumRodjenja(BigInteger datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public String getBrojTelefona() {
        return brojTelefona;
    }

    public void setBrojTelefona(String brojTelefona) {
        this.brojTelefona = brojTelefona;
    }

    public String getEmailAdresa() {
        return emailAdresa;
    }

    public void setEmailAdresa(String emailAdresa) {
        this.emailAdresa = emailAdresa;
    }

    public Integer getRedniBroj() {
        return redniBroj;
    }

    public void setRedniBroj(Integer redniBroj) {
        this.redniBroj = redniBroj;
    }

    @XmlTransient
    public List<Predmet> getPredmetList() {
        return predmetList;
    }

    public void setPredmetList(List<Predmet> predmetList) {
        this.predmetList = predmetList;
    }

    @XmlTransient
    public List<Nastavnikzvanje> getNastavnikzvanjeList() {
        return nastavnikzvanjeList;
    }

    public void setNastavnikzvanjeList(List<Nastavnikzvanje> nastavnikzvanjeList) {
        this.nastavnikzvanjeList = nastavnikzvanjeList;
    }

    public Katedra getKatedraId() {
        return katedraId;
    }

    public void setKatedraId(Katedra katedraId) {
        this.katedraId = katedraId;
    }

    public Zvanje getZvanjeId() {
        return zvanjeId;
    }

    public void setZvanjeId(Zvanje zvanjeId) {
        this.zvanjeId = zvanjeId;
    }

    public Titula getTitulaId() {
        return titulaId;
    }

    public void setTitulaId(Titula titulaId) {
        this.titulaId = titulaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nastavnik)) {
            return false;
        }
        Nastavnik other = (Nastavnik) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domen.Nastavnik[ id=" + id + " ]";
    }

    @Override
    public String vratiNazivKljuca() {
        return "id";
    }

    @Override
    public String vratiNazivTabele() {
        return "nastavnik";
    }

    @Override
    public Object vratiVrednostKljuca() {
        return id;
    }
    
}
