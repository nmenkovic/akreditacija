/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mb;

import db.DatabaseBroker;
import db.Kontroler;
import domen.Katedra;
import domen.Nastavnik;
import domen.Titula;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;


@ManagedBean(name = "nastavnikBean")
@ViewScoped
public class NastavnikBean implements Serializable {

    private Nastavnik nastavnik;
    private List<Nastavnik> nastavnici = new ArrayList<Nastavnik>();
    private List<Nastavnik> filtriraniNastavnici;
    private Nastavnik selektovaniNastavnik;
    private Titula titula;
    private Katedra katedra;

    public NastavnikBean() throws Exception {
        nastavnik = new Nastavnik();
        selektovaniNastavnik = new Nastavnik();
    }

    public Nastavnik getNastavnik() {
        return nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }

    public Katedra getKatedra() {
        return katedra;
    }

    public Titula getTitula() {
        return titula;
    }

    public void setKatedra(Katedra katedra) {
        this.katedra = katedra;
    }

    public void setTitula(Titula titula) {
        this.titula = titula;
    }
    

    public List<Nastavnik> getNastavnici() {
        return nastavnici;
    }

    public void setNastavnici(List<Nastavnik> nastavnici) {
        this.nastavnici = nastavnici;
    }

    public List<Nastavnik> getFiltriraniNastavnici() {
        return filtriraniNastavnici;
    }

    public void setFiltriraniNastavnici(List<Nastavnik> filtriraniNastavnici) {
        this.filtriraniNastavnici = filtriraniNastavnici;
    }

    public Nastavnik getSelektovaniNastavnik() {
        return selektovaniNastavnik;
    }

    public void setSelektovaniNastavnik(Nastavnik selektovaniNastavnik) {
        this.selektovaniNastavnik = selektovaniNastavnik;
    }

    

    public void sacuvajNastavnika() throws Exception {
        String poruka = "";
     
        if (!nastavnik.getEmailAdresa().contains("@") || !nastavnik.getEmailAdresa().contains(".")) {
            poruka += "Niste ispravno uneli email adresu,";
        }
        if (nastavnik.getBrojTelefona()!= null) {
            try {
                Long.parseLong(nastavnik.getBrojTelefona());
            } catch (Exception e) {
                poruka += "Niste ispravno uneli broj telefona,";
            }
        
        if (!poruka.isEmpty()) {
            String[] poruke = poruka.split(",");
            for (String por : poruke) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, por, ""));
            }
        } else {
            try {
                //postaviID();
                Kontroler.vratiObjekat().zapamtiNastavnika(nastavnik);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sistem je zapamtio nastavnika", ""));
              
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Sistem ne može da zapamti nastavnika", ""));
            }
        }
    
        }
    }

   // private void postaviID() throws Exception {
      //  int id = Kontroler.vratiObjekat().vratiMaxKljucNastavnik();
       // nastavnik.setId(id + 1);
   // }

    public List<Nastavnik> vratiNastavnike() {
        try {
            nastavnici = Kontroler.vratiObjekat().vratiSveNastavnike();
            return nastavnici;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Sistem ne može da nađe nastavnike", ""));
            return null;
        }
    }

    public void izmeniNastavnika() throws Exception {
        String poruka = "";
    
        if (!selektovaniNastavnik.getEmailAdresa().contains("@") || !selektovaniNastavnik.getEmailAdresa().contains(".")) {
            poruka += "Niste ispravno uneli email,";
        }
        if (selektovaniNastavnik.getBrojTelefona()!= null) {
            try {
                Long.parseLong(selektovaniNastavnik.getBrojTelefona());
            } catch (Exception e) {
                poruka += "Niste ispravno uneli broj telefona,";
            }
        }
        if (!poruka.isEmpty()) {
            String[] poruke = poruka.split(",");
            for (String por : poruke) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, por, ""));
            }
        } else {
            try {
                Kontroler.vratiObjekat().izmeniNastavnika(selektovaniNastavnik);
                selektovaniNastavnik = new Nastavnik();
                filtriraniNastavnici = null;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sistem je izmenio nastavnika", ""));
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Sistem ne može da izmeni nastavnika", ""));
            }
        }
    }

    public void obrisiNastavnika() {
        try {
            Kontroler.vratiObjekat().izbrisiNastavnika(selektovaniNastavnik);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sistem je obrisao nastavnika", ""));
            selektovaniNastavnik = new Nastavnik();
            filtriraniNastavnici= null;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sistem ne može da obriše nastavnika", ""));
        }
    }

   
}
