/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mb;

import db.DatabaseBroker;
import db.Kontroler;
import domen.Titula;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Komp
 */
@ManagedBean(name = "titulaBean")
@ViewScoped

public class TitulaBean implements Serializable{
private Titula titula;
private List<Titula> titule= new ArrayList<Titula>();
    /**
     * Creates a new instance of IzdavackaKucaBean
     */
    public TitulaBean() {
        titula= new Titula();
    }

    public Titula getTitula() {
        return titula;
    }

    public void setTitula(Titula titula) {
        this.titula = titula;
    }

    public List<Titula> getTitule() {
        return titule;
    }

    public void setTitule(List<Titula> titule) {
        this.titule = titule;
    }
     public List<Titula> vratiTitule() {
        try {
            titule = Kontroler.vratiObjekat().vratiSveTitule();
            return titule;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Sistem ne može da nađe titule", ""));
            return null;
        }
    }
}
