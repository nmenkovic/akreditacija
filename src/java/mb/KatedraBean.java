/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mb;

import db.DatabaseBroker;
import db.Kontroler;
import domen.Katedra;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Komp
 */
@ManagedBean(name = "katedraBean")
@ViewScoped
public class KatedraBean implements Serializable {

    private Katedra katedra;
    private List<Katedra> katedre = new ArrayList<Katedra>();

    /**
     * Creates a new instance of ZanrBean
     */
    public KatedraBean() {
        katedra = new Katedra();
    }

    public Katedra getKatedra() {
        return katedra;
    }

    public void setKatedra(Katedra katedra) {
        this.katedra = katedra;
    }

    public List<Katedra> getKatedre() {
        return katedre;
    }

    public void setKatedre(List<Katedra> katedre) {
        this.katedre = katedre;
    }
     public List<Katedra> vratiKatedre() {
        try {
            katedre = Kontroler.vratiObjekat().vratiSveKatedre();
            return katedre;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Sistem ne može da nađe katedre", ""));
            return null;
        }
    }
}
