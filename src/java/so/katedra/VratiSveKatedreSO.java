/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so.katedra;

import db.DatabaseBroker;
import domen.OpstiDomenskiObjekat;
import java.util.List;
import so.OpstaSO;

/**
 *
 * @author NMenkovic
 */
public class VratiSveKatedreSO extends OpstaSO {

    private List<OpstiDomenskiObjekat> katedre;

    @Override
    public void izvrsiKonkretnuOperaciju(Object objekat) throws Exception {
        katedre = DatabaseBroker.vratiObjekat().vratiSveObjekte((OpstiDomenskiObjekat) objekat);
    }

    public List<OpstiDomenskiObjekat> getKatedre() {
        return katedre;
    }

}
