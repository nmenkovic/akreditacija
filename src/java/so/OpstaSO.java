/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package so;

import db.DatabaseBroker;
import javax.persistence.EntityManager;

/**
 *
 * @author NMenkovic
 */
public abstract class OpstaSO {
     protected EntityManager em;

    public final void izvrsiOperaciju(Object o) throws Exception {
        try {
            DatabaseBroker.vratiObjekat();
            DatabaseBroker.vratiObjekat().zapocniTransakciju();
            izvrsiKonkretnuOperaciju(o);
            DatabaseBroker.vratiObjekat().commit();
        } catch (Exception e) {
            DatabaseBroker.vratiObjekat().rollback();
            throw e;
        } finally {
            DatabaseBroker.vratiObjekat().zatvoriEntityManager();
        }
    }

    public abstract void izvrsiKonkretnuOperaciju(Object objekat) throws Exception;
}
