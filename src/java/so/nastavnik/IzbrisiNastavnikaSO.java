/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package so.nastavnik;

import db.DatabaseBroker;
import domen.OpstiDomenskiObjekat;
import so.OpstaSO;

/**
 *
 * @author NMenkovic
 */
public class IzbrisiNastavnikaSO extends OpstaSO {

    @Override
    public void izvrsiKonkretnuOperaciju(Object objekat) throws Exception {
        DatabaseBroker.vratiObjekat().obrisiObjekat((OpstiDomenskiObjekat) objekat);
    }
    
}
