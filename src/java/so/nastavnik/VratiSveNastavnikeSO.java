/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package so.nastavnik;

import db.DatabaseBroker;
import domen.OpstiDomenskiObjekat;
import java.util.List;
import so.OpstaSO;

/**
 *
 * @author NMenkovic
 */
public class VratiSveNastavnikeSO extends OpstaSO {
    private List<OpstiDomenskiObjekat> nastavnici;

    @Override
    public void izvrsiKonkretnuOperaciju(Object objekat) throws Exception {
        nastavnici = DatabaseBroker.vratiObjekat().vratiSveObjekte((OpstiDomenskiObjekat) objekat);
    }

    public List<OpstiDomenskiObjekat> getNastavnici() {
        return nastavnici;
    }

}
