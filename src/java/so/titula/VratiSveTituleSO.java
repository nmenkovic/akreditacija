/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package so.titula;

import db.DatabaseBroker;
import domen.OpstiDomenskiObjekat;
import java.util.List;
import so.OpstaSO;

/**
 *
 * @author NMenkovic
 */
public class VratiSveTituleSO extends OpstaSO{
    private List<OpstiDomenskiObjekat> titule;

    @Override
    public void izvrsiKonkretnuOperaciju(Object objekat) throws Exception {
        titule = DatabaseBroker.vratiObjekat().vratiSveObjekte((OpstiDomenskiObjekat) objekat);
    }

    public List<OpstiDomenskiObjekat> getTitule() {
        return titule;
    }

}
