/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package so.zvanje;

import db.DatabaseBroker;
import domen.OpstiDomenskiObjekat;
import java.util.List;
import so.OpstaSO;

/**
 *
 * @author NMenkovic
 */
public class VratiSvaZvanjaSO extends OpstaSO {
    private List<OpstiDomenskiObjekat> zvanja;

    @Override
    public void izvrsiKonkretnuOperaciju(Object objekat) throws Exception {
        zvanja = DatabaseBroker.vratiObjekat().vratiSveObjekte((OpstiDomenskiObjekat) objekat);
    }

    public List<OpstiDomenskiObjekat> getZvanja() {
        return zvanja;
    }

}
