/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import domen.Katedra;
import domen.Nastavnik;
import domen.Oblast;
import domen.OpstiDomenskiObjekat;
import domen.Predmet;
import domen.Titula;
import domen.Zvanje;
import java.util.ArrayList;
import java.util.List;
import so.VratiMaxKljucSO;
import so.katedra.VratiSveKatedreSO;
import so.nastavnik.IzbrisiNastavnikaSO;
import so.nastavnik.IzmeniNastavnikaSO;
import so.nastavnik.VratiSveNastavnikeSO;
import so.nastavnik.ZapamtiNastavnikaSO;
import so.titula.VratiSveTituleSO;
import so.zvanje.VratiSvaZvanjaSO;

/**
 *
 * @author NMenkovic
 */
public class Kontroler {

    private static Kontroler objekat;

    private Kontroler() {
    }

    public static Kontroler vratiObjekat() {
        if (objekat == null) {
            objekat = new Kontroler();
        }
        return objekat;
    }

    public List<Titula> vratiSveTitule() throws Exception {
        try {
            List<Titula> titule = new ArrayList<Titula>();
            VratiSveTituleSO vratiSveTitule = new VratiSveTituleSO();
            vratiSveTitule.izvrsiOperaciju(new Titula());
            List<OpstiDomenskiObjekat> objekti = vratiSveTitule.getTitule();
            for (OpstiDomenskiObjekat odo : objekti) {
                titule.add((Titula) odo);
            }
            return titule;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Katedra> vratiSveKatedre() throws Exception {
        try {
            List<Katedra> katedre = new ArrayList<Katedra>();
            VratiSveKatedreSO vratiSveKatedre = new VratiSveKatedreSO();
            vratiSveKatedre.izvrsiOperaciju(new Katedra());
            List<OpstiDomenskiObjekat> objekti = vratiSveKatedre.getKatedre();
            for (OpstiDomenskiObjekat odo : objekti) {
                katedre.add((Katedra) odo);
            }
            return katedre;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Zvanje> vratiSvaZvanja() throws Exception {
        try {
            List<Zvanje> zvanja = new ArrayList<Zvanje>();
            VratiSvaZvanjaSO vratiSvaZvanja = new VratiSvaZvanjaSO();
            vratiSvaZvanja.izvrsiOperaciju(new Zvanje());
            List<OpstiDomenskiObjekat> objekti = vratiSvaZvanja.getZvanja();
            for (OpstiDomenskiObjekat odo : objekti) {
                zvanja.add((Zvanje) odo);
            }
            return zvanja;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Nastavnik> vratiSveNastavnike() throws Exception {
        try {
            List<Nastavnik> nastavnici = new ArrayList<Nastavnik>();
            VratiSveNastavnikeSO vratiSveNastavnike = new VratiSveNastavnikeSO();
            vratiSveNastavnike.izvrsiOperaciju(new Nastavnik());
            List<OpstiDomenskiObjekat> objekti = vratiSveNastavnike.getNastavnici();
            for (OpstiDomenskiObjekat odo : objekti) {
                nastavnici.add((Nastavnik) odo);
            }
            return nastavnici;
        } catch (Exception e) {
            throw e;
        }
    }

    public void zapamtiNastavnika(Nastavnik nastavnik) throws Exception {
        try {
            ZapamtiNastavnikaSO zapamtiNastavnika = new ZapamtiNastavnikaSO();
            zapamtiNastavnika.izvrsiOperaciju(nastavnik);
        } catch (Exception e) {
            throw e;
        }
    }

    public void izbrisiNastavnika(Nastavnik nastavnik) throws Exception {
        try {
            IzbrisiNastavnikaSO izbrisiNastavnika = new IzbrisiNastavnikaSO();
            izbrisiNastavnika.izvrsiOperaciju(nastavnik);
        } catch (Exception e) {
            throw e;
        }
    }

    public void izmeniNastavnika(Nastavnik nastavnik) throws Exception {
        try {
            IzmeniNastavnikaSO izmeniNastavnika = new IzmeniNastavnikaSO();
            izmeniNastavnika.izvrsiOperaciju(nastavnik);
        } catch (Exception e) {
            throw e;
        }
    }
    
     public int vratiMaxKljucNastavnik() throws Exception {
        try {
            VratiMaxKljucSO maxKljuc = new VratiMaxKljucSO();
            maxKljuc.izvrsiOperaciju(new Nastavnik());
            int kljuc = maxKljuc.getKljuc();
            return kljuc;
        } catch (Exception e) {
            throw e;
        }
    }

}
