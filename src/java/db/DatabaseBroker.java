/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import domen.Katedra;
import domen.Nastavnik;
import domen.Oblast;
import domen.OpstiDomenskiObjekat;
import domen.Predmet;
import domen.Titula;
import domen.Zvanje;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author NMenkovic
 */
public class DatabaseBroker {

    private static DatabaseBroker objekat;
    private static EntityManagerFactory emf;
    private EntityManager em;

    public static DatabaseBroker vratiObjekat() {
        if (objekat == null) {
            objekat = new DatabaseBroker();
        }
        return objekat;
    }

    private DatabaseBroker() {
        emf = Persistence.createEntityManagerFactory("AkreditacijaPU");
    }

    public void zapocniTransakciju() {
        em = emf.createEntityManager();
        em.getTransaction().begin();
    }

    public void zatvoriEntityManager() {
        em.close();
    }

    public void commit() throws Exception {
        try {
            em.flush();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new Exception("Greska prilikom commit-a transakcije!");
        }
    }

    public void rollback() throws Exception {
        try {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        } catch (Exception e) {
            throw new Exception("Greska prilikom rollback-a transakcije!");
        }
    }

    public void sacuvajObjekat(OpstiDomenskiObjekat odo) throws Exception {
        em.persist(odo);
    }

    public void izmeniObjekat(OpstiDomenskiObjekat odo) throws Exception {
        em.merge(odo);
    }

    public void obrisiObjekat(OpstiDomenskiObjekat odo) throws Exception {
        Query q = em.createQuery("DELETE FROM " + odo.vratiNazivTabele() + " WHERE " + odo.vratiNazivKljuca() + " = :id");
        q.setParameter("id", odo.vratiVrednostKljuca()).executeUpdate();
    }

    public List<OpstiDomenskiObjekat> vratiSveObjekte(OpstiDomenskiObjekat odo) throws Exception {
        System.out.println("dbbroker");
        List<OpstiDomenskiObjekat> lista = em.createNamedQuery(odo.getClass().getSimpleName() + ".findAll").getResultList();
        return lista;
    }

    public int vratiMaxKljuc(OpstiDomenskiObjekat odo) {
        Query q = em.createQuery("SELECT max(" + odo.vratiNazivKljuca() + ") FROM " + odo.vratiNazivTabele() + "");
        Object kljuc = q.getSingleResult();
        if (kljuc != null) {
            return Integer.parseInt(kljuc.toString());
        } else {
            return 0;
        }
    }
}
